### 介绍

此仓库为奇特物联(iotkit)物联网平台的智能家居小程序项目。 

系统包含了品类、物模型、消息转换、通讯组件（mqtt通讯组件、小度音箱接入组件、onenet Studio接入组件）、云端低代码设备开发、设备管理、规则引擎、第三方平台接入、数据流转、数据可视化、报警中心等模块和智能家居APP（小程序）。

 **前端项目见：** https://gitee.com/iotkit-open-source/iot-console-web

 **演示地址：** [演示地址](http://120.76.96.206)，账号：guest1,密码：guest123  (只读权限)

 **智能家居小程序：** https://gitee.com/iotkit-open-source/iot-mp-home 


### 技术栈
使用的框架和组件包含vue3.0、Taro3.x、NutUi3.x等


### 使用说明

1.  文档
    http://iotkit-open-source.gitee.io/document/

2.  安装 

    `npm install -g @tarojs/cli`

    `npm install`


2.  启动和编译 

    `npm run dev:weapp` 

    `npm run build:weapp` 

3.  用微信开发者工具导入dist目录



### 界面截图
![输入图片说明](doc/0.png)
![输入图片说明](doc/1.png)
![输入图片说明](doc/2.png)
![输入图片说明](doc/3.png)
![输入图片说明](doc/4.png)
![输入图片说明](doc/5.png)