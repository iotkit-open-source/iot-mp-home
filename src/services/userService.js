import Taro from "@tarojs/taro";
import httpService from "./httpService"

const user={
    currentHome:function(){
        return Taro.getStorageSync("current_home");
    },
    fetchHome:function(){
        //取用户当前家庭
        httpService.get("/space/currentHome").then(res=>{
            Taro.setStorage({
                key: "current_home",
                data: res.data,
            });
        });

    }
}




export default user;